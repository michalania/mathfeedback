package gameFeedback

import java.util
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.LongAdder

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers
import org.scalacheck.Arbitrary._
import org.scalacheck.Prop
import org.scalacheck.Prop._
import org.scalatest.exceptions.TestFailedException

import scala.collection.immutable
import scala.reflect.internal.util.Collections

object CoinAction extends Enumeration {
  type CoinAction = Value
  val ThrowCoin = Value
}

import gameFeedback.CoinAction._

object CoinConstants {
  val HEAD: Int = 1
  val TAIL: Int = 0
  val EDGE: Int = -1
  val EDGE_CHANCE: Double = 0.001
  val FLAT_CHANCE: Double = (1.0 - EDGE_CHANCE) / 2
}

class CoinGameState(val coinValue: Int) extends GameState[CoinAction, CoinGameState] {
  import CoinConstants._

  override def gameOver: Boolean = coinValue != EDGE
  override def value: Double = if (coinValue == EDGE) 0.2 else coinValue
  override def moves: Map[CoinAction, Seq[Move]] = Map[CoinAction, Seq[Move]](ThrowCoin ->
    List(move(ThrowCoin, new CoinGameState(HEAD), FLAT_CHANCE),
      move(ThrowCoin, new CoinGameState(TAIL), FLAT_CHANCE),
      move(ThrowCoin, new CoinGameState(EDGE), EDGE_CHANCE)))


  def canEqual(other: Any): Boolean = other.isInstanceOf[CoinGameState]

  override def equals(other: Any): Boolean = other match {
    case that: CoinGameState =>
      (that canEqual this) &&
        coinValue == that.coinValue
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(coinValue)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

class StatCalculator[Action >: Null, State <: GameState[Action, State]] extends BasicDataCalculator[Action, State, BasicData[Action]] {
  val entries = new ConcurrentHashMap[Int, LongAdder](30)
  val width = new ConcurrentHashMap[Int, LongAdder](30)

  override def initialPost: List[Operand] => Unit = (ops: List[Operand]) => {
    val old = ops.head
    entries.computeIfAbsent(old.level, l => new LongAdder).increment
    width.computeIfAbsent(old.level, l => new LongAdder).add(old.next.values.flatten.count(! _._2.state.gameOver))
    super.initialPost(ops)
  }

  override def toString = {
    val m = new util.HashMap[Int, Long](30)
    width.forEach((k: Int, v: LongAdder) => m.put(k, v.longValue / entries.get(k).longValue))
    "entries: " + entries + "\nwidth: " + m
  }
}

class ClashTester(val clash: ClashInTheShadyWoods) {
  type Data = BasicData[ClashAction.ClashAction]
  type Tree = GameTree[ClashAction.ClashAction, clash.State, Data]

  def newTree = new Tree(clash.initialState, () => new Data, 512 * 1024, 12)

  def parTest = {
    val calculator = new StatCalculator[ClashAction.ClashAction, clash.State]
    val tree = newTree

    val startTime = System.currentTimeMillis()
    val d = tree.foreachPostPar(calculator.initialPost)
    val endTime = System.currentTimeMillis()

    println("parTest time:" + (endTime - startTime))
    println(calculator)
    println("nodes:" + tree.nodes.size())

    tree
  }

  def singleTest = {
    val calculator = new StatCalculator[ClashAction.ClashAction, clash.State]
    val tree = newTree

    val startTime = System.currentTimeMillis()
    val d = tree.foreachPost(calculator.initialPost)
    val endTime = System.currentTimeMillis()

    println("singleTest time:" + (endTime - startTime))
    println(calculator)
    println("nodes:" + tree.nodes.size())

    (tree, calculator)
  }
}

@RunWith(classOf[JUnitRunner])
class GameTreeTest extends FunSuite with Checkers {
  test("Calculate CoinGame") {
    println("Calculate CoinGame")
    val coinTree = new GameTree[CoinAction, CoinGameState, BasicData[CoinAction]](new CoinGameState(CoinConstants.EDGE),
      () => new BasicData[CoinAction],
      10,
      10
    )

    val calculator = new StatCalculator[CoinAction, CoinGameState]

    val data = coinTree.foreachPost(calculator.initialPost).data
    assert(coinTree.nodes.size() == 3,"only 3 nodes")
    assert(data.value == 0.5,"chance to win: 0.5")
    assert(data.best == ThrowCoin, "only single action")
    println("calculator.entries:" + calculator.entries)
  }

  test("Clash: single only white") {
    println
    println("Clash: single only white")

    val exec = new ClashTester(new ClashInTheShadyWoods(".n.s.")).singleTest
    val whiteTree = exec._1
    val calculator = exec._2

    assert(whiteTree.root.data.value == 1,"always win")
    println(whiteTree.initial)
    println("calculator.entries:" + calculator.entries)
    assert(calculator.entries.size() == 3)
  }

  test("Clash: par only white") {
    println
    println("Clash: par only white")
    val whiteTree = new ClashTester(new ClashInTheShadyWoods(".n.s.")).parTest
    assert(whiteTree.root.data.value == 1,"always win")
  }

  test("Clash single: only black") {
    println
    println("Clash single: only black")
    val exec = new ClashTester(new ClashInTheShadyWoods("#N##S#")).singleTest
    val blackTree = exec._1
    val calculator = exec._2

    assert(blackTree.root.data.value == 0,"no chance to win")
    println(blackTree.initial)
    assert(calculator.entries.size() == 4)
  }


  test("Clash par: only black") {
    println
    println("Clash par: only black")
    val blackTree = new ClashTester(new ClashInTheShadyWoods("#N##S#")).parTest

    assert(blackTree.root.data.value == 0, "no chance to win")
    println(blackTree.initial)
  }

  test("Clash par") {
    println
    println("Clash par")
    val tester = new ClashTester(new ClashInTheShadyWoods(".N.#.#.##.#..#.#.#s#"))
    val singleData = tester.singleTest._1.root.data
    System.gc()
    val parData = tester.parTest.root.data

    println("singleData:" + singleData)
    println("parData:" + parData)
  }
}
