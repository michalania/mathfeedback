package gameFeedback

import scala.collection.mutable

case class CountValue(sum: Double, chance: Double, min: Double, max: Double, count: Long) {
  def this(v: Double, c: Double) = this(v * c, c, v, v, 1)
  def this() = this(0, 0, Double.MaxValue, Double.MinValue, 0)

  def isEmpty: Boolean = chance == 0
  def value: Double = if (isEmpty) 0 else (sum / chance)
  def avg: Double = if (count == 0) 0 else (sum / count)

  def plus(v: Double, c: Double): CountValue = if (isEmpty) new CountValue(v, c) else
    CountValue(sum + v * c, chance + c, Math.min(v, min), Math.max(v, max), count + 1)

  def aggregate(v: CountValue, c: Double): CountValue = if (v.isEmpty) this
    else if (isEmpty) CountValue(v.sum * c, v.chance * c, v.min, v.max, v.count) else
    CountValue(sum + v.value * c, chance + c, Math.min(min, v.min), Math.max(max, v.max), Math.max(count, v.count) + 1)

  def absIncrease(v: Double): CountValue = if (isEmpty) this else
    CountValue(sum + v * chance, chance, min + v, max + v, count)

  def absScale(v: Double): CountValue = if (isEmpty) this else {
    val mi = if (v > 0) min * v else max * v
    val ma = if (v > 0) max * v else min * v
    CountValue(sum * v, chance, mi, ma, count)
  }

  def normalize: CountValue = if (isEmpty) this else CountValue(value, 1, min, max, count)

  import GameDefs.pretty
  override def toString: String = if (isEmpty) "CountValue:empty" else
    "value:" + pretty(value) + " [" + pretty(min) + "   " + pretty(max) + "] " + " count:" + count
}

object CountValueDefs {
  val emptyVal: CountValue = new CountValue
  val zeroDepthVal: CountValue = new CountValue(0,1)
}

class BasicData[Action >: Null] {
  import CountValueDefs._

  var countValue: CountValue = emptyVal
  var value: Double = Double.MinValue
  var best: Action = _
  var choiceValue: Map[Action, CountValue] = Map.empty
  var depth: CountValue = emptyVal
  var optimalDepth: CountValue = emptyVal
  var lossValue: CountValue = emptyVal
  var lossRatio: CountValue = emptyVal
  var lossValueMed: CountValue = emptyVal
  var lossRatioMed: CountValue = emptyVal
  var fullyVisited: Boolean = false
  var hitLossValue: CountValue = zeroDepthVal
  var hitLossRatio: CountValue = zeroDepthVal

  import GameDefs.pretty

  override def toString: String =
    "Data:\n  choiceValue: " + choiceValue +
      "\n  countValue: " + countValue +
      "\n  value: " + pretty(value) + "  best: " + best +
      "\n  fullyVisited: " + fullyVisited +
      "\n  depth: " + depth +
      "\n  optimalDepth: " + optimalDepth +
      "\n  lossValue: " + lossValue +
      "\n  lossRatio: " + lossRatio +
      "\n  lossValueMed: " + lossValueMed +
      "\n  lossRatioMed: " + lossRatioMed +
      "\n  hitLossValue: " + hitLossValue +
      "\n  hitLossRatio: " + hitLossRatio
}

class BasicDataCalculator[Action >: Null, State <: GameState[Action, State], Data <: BasicData[Action]]() {
  type Tree = GameTree[Action, State, Data]
  type Info = (Tree#Move, Tree#Node)
  type Operand = Tree#Operand

  import CountValueDefs._

  def data(info: Info): BasicData[Action] = info._2.data

  def initialPost: List[Operand] => Unit = (ops: List[Operand]) => {
    val act = ops.head

    if (act.state.gameOver) { // end state
      act.data.value = act.state.value
      act.data.fullyVisited = true
      act.data.depth = zeroDepthVal
      act.data.optimalDepth = zeroDepthVal
      act.data.countValue = new CountValue(act.data.value, 1)
    } else if (act.next.isEmpty) { // heuristics
      act.data.value = act.state.value
      act.data.countValue = new CountValue(act.data.value, 1)
      act.data.depth = new CountValue(act.level,1)
      act.data.optimalDepth = new CountValue(act.level,1)
    } else { // inner state
      act.data.fullyVisited = true
      act.data.choiceValue = for {(a, mns) <- act.next}
        yield a -> mns.foldLeft(emptyVal)((o: CountValue, m: Info) => o.plus(data(m).value, m._1.chance)).normalize

      val (best, bestValue) = act.data.choiceValue.maxBy({case (_,c) => (c.value, c.min, c.max)})

      act.data.countValue = bestValue
      act.data.best = best
      act.data.value = bestValue.value

      act.data.depth = act.next.values.flatten
        .foldLeft(emptyVal)((o: CountValue, m: Info) => o.aggregate(data(m).depth, m._1.chance))
        .normalize
        .absIncrease(1)

      act.data.optimalDepth = act.next(best)
        .foldLeft(emptyVal)((o: CountValue, m: Info) => o.aggregate(data(m).optimalDepth, m._1.chance))
        .normalize
        .absIncrease(1)

      // we will think about the outcome of suboptimal move
      // let's consider non final states
      if (act.next.size > 1 && // there can be suboptimal move
          bestValue.min < GameDefs.WIN &&
          bestValue.max > GameDefs.LOOSE) { // and neither win nor loose is obvious
        val interestingChoices = act.data.choiceValue
            .filter({case (a, v) => a != best && v.max != GameDefs.LOOSE})
        if (interestingChoices.nonEmpty) {
          act.data.lossValue = interestingChoices.values
            .foldLeft(emptyVal)((o: CountValue, v: CountValue) => o.plus(act.data.value - v.value, 1))
            .normalize
          act.data.lossRatio = act.data.lossValue.absScale(1/act.data.value)
        }
      }

      act.data.lossValueMed = act.next.values.flatten
        .foldLeft(act.data.lossValue)((o: CountValue, m: Info) => o.aggregate(data(m).lossValueMed, m._1.chance))
        .normalize
      act.data.lossRatioMed = act.next.values.flatten
        .foldLeft(act.data.lossRatio)((o: CountValue, m: Info) => o.aggregate(data(m).lossRatioMed, m._1.chance))
        .normalize

      act.data.hitLossValue = act.next.values.flatten
        .foldLeft(emptyVal)((o: CountValue, m: Info) => o.aggregate(data(m).hitLossValue, m._1.chance))
        .normalize
        .absIncrease(act.data.lossValue.value)
      act.data.hitLossRatio = act.next.values.flatten
        .foldLeft(emptyVal)((o: CountValue, m: Info) => o.aggregate(data(m).hitLossRatio, m._1.chance))
        .normalize
        .absIncrease(act.data.lossRatio.value)
    }
  }
}