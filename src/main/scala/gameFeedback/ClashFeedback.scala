package gameFeedback

import scala.collection.{JavaConverters, mutable}
import scala.reflect.internal.util.Collections

class ClashData extends BasicData[ClashAction.ClashAction] {

}

class ClashFeedback(val game: ClashInTheShadyWoods, val comment: String) {
  type Tree = GameTree[ClashAction.ClashAction, game.State, ClashData]
  import GameDefs.pretty

  class ClashDataCalculator()
    extends BasicDataCalculator[ClashAction.ClashAction, game.State, ClashData] {

    val naiveDepth: Double =
      if (game.whiteToAll == game.allTiles) 0 else 1.0 / (1 - game.whiteToAll)

    override def initialPost: List[Operand] => Unit = (ops: List[Operand]) => {
      super.initialPost(ops)
      val act = ops.head
      if (!act.state.gameOver && act.next.isEmpty) { // heuristics
        act.data.depth = new CountValue(naiveDepth, 1)
        act.data.optimalDepth = new CountValue(1, 1)
      }
    }
  }

  val calculator = new ClashDataCalculator

  val tree = new Tree(game.initialState,
    () => new ClashData,128 * 1024, 15)

  def calculateInfo = {
    val start = System.currentTimeMillis()
    val op = tree.foreachPostPar(calculator.initialPost)
    val end = System.currentTimeMillis()

    println()
    println(game.shadyPath + " " + comment)
    println("  time: " + (end - start))
    println("  nodes:" + tree.nodes.size)
    println("  naiveDepth: " + pretty(calculator.naiveDepth))
    val scalaNodes = JavaConverters.mapAsScalaMap(tree.nodes)
    val nonTrivial = scalaNodes.values.count(!_.data.lossValue.isEmpty)
    println("  nodes with non trivial choice:" + nonTrivial)
    println("  % non Trivial:" + pretty(nonTrivial * 100 / tree.nodes.size))
    println("  not fully visited:" + scalaNodes.values.count(!_.data.fullyVisited))
    println("  naive value:" + game.naiveValue)
    println(op.data)
  }

  def show = {
    val d = tree.root.data
    println(game.shadyPath + " " + comment)
    println(" value:" + d.countValue + "  hitLoss: " + d.hitLossValue + " hitLossRatio: " + d.hitLossRatio)
  }
}

object ClashFeedbackMain {
  def main(args: Array[String]): Unit = {
    val feedbacks = Array(
      new ClashFeedback(new ClashInTheShadyWoods(".N.#.#.##.#..#.#.#s#"), "podstawowy"),
      new ClashFeedback(new ClashInTheShadyWoods(".N.#.#.##.#.#..#.#.#s#"), "podstawowy, troche dluzszy"),
      new ClashFeedback(new ClashInTheShadyWoods("#n##..#.#.##.#.#..##s#"), "podstawowy, bardziej czarny"),
      new ClashFeedback(new ClashInTheShadyWoods("#n##.##.#.#..#.#.##.##s#"), "jak w wyborze"),
      new ClashFeedback(new ClashInTheShadyWoods("#n#.#.#..#.#.##.###s#"), ""),

      new ClashFeedback(new FreeChoiceClash(".N.#.#.##.#..#.#.#s#"), "wybor, podstawowy"),
      new ClashFeedback(new FreeChoiceClash(".N.#.#.##.#.#..#.#.#s#"), "wybor, troche dluzszy"),
      new ClashFeedback(new FreeChoiceClash("#n##..#.#.##.#.#..##s#"), "wybor, bardziej czarny"),
      new ClashFeedback(new FreeChoiceClash("#n#..#..#.#.#.##.##s#"), "wybor bardziej czarny"),
      new ClashFeedback(new FreeChoiceClash("#n##.##.#.#..#.#.##.##s#"), "wybor"),
      new ClashFeedback(new FreeChoiceClash("#n#..#.#.#.#.##.##s#"), "wybor"),
      new ClashFeedback(new FreeChoiceClash(".N.##.##.#.#.#.##.##s#"), "ciemno wybor")
    )

    feedbacks.foreach(_.calculateInfo)
    feedbacks.foreach(_.show)
  }
}
