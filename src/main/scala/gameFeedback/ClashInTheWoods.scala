package gameFeedback

object ClashAction extends Enumeration {
  type ClashAction = Value
  val RandAction, StepAction = Value
}

import ClashAction._

/**
  * Rules for the game clash in the woods: see game description at:
  * http://students.mimuw.edu.pl/~mw197884/ClashInTheShadyWoods.png
  *
  * @param shadyPath gameboard:
  *                  . sunny tile
  *                  # shadow place
  *                  n thug start (in sun)
  *                  N thug start (in shadow)
  *                  s samurai start (in sun)
  *                  S samurai start (in shadow)
  */
class ClashInTheShadyWoods(val shadyPath: String) {
  val samurai: Int = shadyPath.indexWhere((p:Char) => p == 's' || p == 'S')
  val thug: Int = shadyPath.indexWhere((p:Char) => p == 'n' || p == 'N')

  val tiles: Array[Boolean] = {
    if (shadyPath.filter((p:Char) => p == 'n' || p == 'N').length != 1)
      throw new Exception("Excpeting single start point for thug - only one 'n' or 'N'")
    if (shadyPath.filter((p:Char) => p == 's' || p == 'S').length != 1)
      throw new Exception("Excpeting single start point for samurai - only one 's' or 'S'")
    if (!shadyPath.matches("[.#sSnN]+"))
      throw new Exception("Not allowed characters, allowed only: .#sSnN")

    val t: Array[Boolean] = new Array(shadyPath.length)
    for (i: Int <- 0 until shadyPath.length) t(i) = (shadyPath(i) == 's' || shadyPath(i) == 'n' || shadyPath(i) == '.')
    t
  }

  val allTiles: Double = tiles.length + 6 // additional tiles outside of the map
  val whiteTiles: Double = tiles.count(p => p) + // white tiles
    (if (tiles(0)) 3 else 0) + // bonus for additional tiles from the left side
    (if (tiles(tiles.length - 1)) 3 else 0) + // bonus for additional tiles from the right side
    1 // bonus for being able to select the single step every second roundS
  val whiteToAll: Double = whiteTiles / allTiles

  val naiveValue: Double = {
    val w = whiteToAll

    // value(w) = w^2 (both end on white) + value(w) * 2 (w * (1 - w)) (both end on different color)
    // value(w) = w^2 + value(v) * (2w - 2 w^2)
    // value(w) *(1 - 2w + 2w^2) = w^2
    // value(w) = w^2 / ( 1 - 2w + 2w^2)

    val n = w * w
    val d = 1 - 2 * w + 2 * w * w

    if (d == 0) 1 else Math.min(n/d, 1)
  }

  def sunny(place: Int): Boolean =
    if (place < 0) tiles(0)
    else if (place >= tiles.length) tiles(tiles.length - 1)
    else tiles(place)

  private val oneDie: Double = 1.0 / 6.0
  private val twoDice: Double = oneDie / 6.0
  private val samuraiRandArray = Array[Int](6, 1, 4, 5, 2, 3)
  private val thugRandArray = Array[Int](6, 5, 2, 1, 4, 3)

  def canChooseNext(clashAction: ClashAction): Boolean = clashAction != StepAction

  def initialState: State = new State(samurai, thug, false, false, 0, 0)

  class State(val samurai: Int,
              val thug: Int,
              canChoose: Boolean,
              swordStrike: Boolean,
              samuraiRolls: Int,
              thugRolls: Int) extends GameState[ClashAction, State] {

    def strike: Boolean = swordStrike || samurai == thug
    def hasChoice: Boolean = !gameOver && samuraiRolls != 1 && canChoose
    def samuraiRoll: Int = if (gameOver) 0 else samuraiRolls
    def thugRoll: Int = if (gameOver) 0 else thugRolls

    override def gameOver: Boolean = strike && sunny(samurai) == sunny(thug)

    override def value: Double = if (gameOver) {
      if (sunny(samurai)) GameDefs.WIN else GameDefs.LOOSE
    } else naiveValue

    override def toString: String = "s: " + samurai +",roll:" +  samuraiRoll +
      " n:" + thug + ",roll:" + thugRoll +
      " choice:" + hasChoice
      " end:" + gameOver
      " value:" + value

    override def moves: Map[ClashAction, Seq[Move]] =
      if (gameOver) Map.empty
      else {
        val willSamurai = nextSamurai(samuraiRoll)
        val willThug = nextThug
        val willStrike = (samurai > thug) != (willSamurai > willThug)
        val endRand = willStrike && sunny(willSamurai) == sunny(willThug)
        val stepSamurai = nextSamurai(1)
        val stepStrike = (samurai > thug) != (stepSamurai > willThug)
        val endStep = stepStrike && sunny(stepSamurai) == sunny(willThug)

        if (!hasChoice)
          if (endRand)
            Map(RandAction -> Array(move(RandAction,
              new State(willSamurai, willThug,false, true, 0, 0),
             1)))
          else
            Map(RandAction -> movesAfterAction(willSamurai, willThug, willStrike, RandAction))
        else {
          val rands: Seq[Move] = if (endRand) Array(move(RandAction,
              new State(willSamurai, willThug,false, true, 0, 0),
             1))
            else
              movesAfterAction(willSamurai, willThug, willStrike, RandAction)
          val steps: Seq[Move] = if (endStep) Array(move(StepAction,
              new State(stepSamurai, willThug, false, true, 0, 0),
              chance = 1))
            else
              movesAfterAction(stepSamurai, willThug, stepStrike, StepAction)
          Map(RandAction -> rands, StepAction -> steps)
        }
      }

    private def nextSamurai(steps: Int): Int = if (thug > samurai) samurai + steps else samurai - steps
    private def nextThug: Int = if (thug > samurai) thug - thugRoll else thug + thugRoll

    private def movesAfterAction(willSamurai: Int, willThug: Int, willStrike: Boolean, action: ClashAction) =
      for {s <- samuraiRandArray; t <- thugRandArray}
        yield move(action, new State(willSamurai, willThug, canChooseNext(action), willStrike, s, t), twoDice)

    def canEqual(other: Any): Boolean = other.isInstanceOf[State]

    override def equals(other: Any): Boolean = other match {
      case that: State =>
        (that canEqual this) &&
          samurai == that.samurai &&
          thug == that.thug &&
          strike == that.strike &&
          samuraiRoll == that.samuraiRoll &&
          hasChoice == that.hasChoice &&
          thugRoll == that.thugRoll
      case _ => false
    }

    override def hashCode(): Int = {
      val state = Seq(samurai, thug, strike, hasChoice, samuraiRoll, thugRoll)
      state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
    }
  }

}

class FreeChoiceClash(shadyPath: String) extends ClashInTheShadyWoods(shadyPath) {
  override def canChooseNext(clashAction: ClashAction): Boolean = true
}
