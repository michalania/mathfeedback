package gameFeedback

object GameDefs {
  val WIN: Double = 1.0
  val LOOSE: Double = 0.0

  def pretty(x: Double): String = new java.text.DecimalFormat("0.###").format(x)
}

/**
  * Game state, should be able to list avable moves or inform if the game is over
  * @tparam Action action that can be performed in the game
  * @tparam State the game state itself
  */
trait GameState[Action, State <: GameState[Action, State]] {
  /** Is this move the end of the game */
  def gameOver: Boolean

  /** Value of player score if it's a end state, otherwise some heuristic */
  def value: Double

  /**
    * One of available moves game from some GameState
    *
    * @param action player choice to get into the state
    * @param state state of the game after the move
    * @param chance the probability of being in the state after the action is performed
    */
  case class Move(action: Action, state: State, chance: Double) {
    def normalize(other: State): Move = if (state eq other) this else new Move(action, other, chance)
  }

  def move(action: Action, state: State, chance: Double) = new Move(action, state, chance)

  /** Seq of available moves from the state */
  def moves: Map[Action, Seq[Move]]

  def interesting: Boolean = false
}
