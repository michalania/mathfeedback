package gameFeedback

import java.util.concurrent.ConcurrentHashMap

class GameTree[Action >: Null, State <: GameState[Action, State], Data](val initial: State,
                                                                        val dataSupplier: () => Data,
                                                                        var maxStates: Int,
                                                                        var maxDepth: Int) {
  type Move = State#Move
  val nodes: ConcurrentHashMap[State, Node] = new ConcurrentHashMap[State, Node](maxStates / 2)
  val root: Node = new Node(initial)
  var currentCalculation: Int = 0

  /**
    * Operand for game data calculations
    * @param next Seq of the next moves with their associated data
    * @param level depth in the game tree
    */
  class Operand(val move: Move,
                val node: Node,
                val next: Map[Action, Seq[(Move, Node)]],
                val level: Int) {
    /** current game state */
    def state: State = node.state
    /** data connected to current game state */
    def data: Data = node.data
  }

  type Operands = List[Operand]

  class Node(val state: State) {
    val data: Data = dataSupplier.apply

    /** Used used to decide if repeat the postfix calculation for the node
      * if calculation was sone for a node deeper in the tree it will be recalculated
      * and the variable will be updated. Otherwise the already calculated value will be used.
      */
    def lowest: Int = lowestLevelCalc
    private[GameTree] var lowestLevelCalc: Int = Int.MaxValue
    private[GameTree] var calculation: Int = Int.MinValue
    def alreadyDone(level: Int): Boolean = currentCalculation == calculation && lowest <= level
    def markDone(level: Int) = {
      lowestLevelCalc = level
      calculation = currentCalculation
    }
  }

  /**
    * Visit states of the game and calls calc for each of them before getting deeper in the tree
    * @param calc calculation to perform, should setup results in operands data member
    *             List[Operand] is a from the current node of game tree execution to the tree root
    *             head of the list is the current operand
    */
  def foreachPre(calc: List[Operand] => Unit): Data = {
    foreachPre(calc, List(initialOperand))
    root.data
  }

  private def foreachPre(calc: List[Operand] => Unit, ops: List[Operand]): Unit = {
    val old = ops.head
    if (ops.tail.exists(_.node.state == old.state)) ()
    else {
      calc(ops)
      for ((move, node) <- old.next.values.flatten)
        foreachPre(calc, operand(ops, move, node) :: ops)
    }
  }

  /**
    * Visit states of the game and calls calc for each of them before getting deeper in the tree
    * @param calc calculation to perform, should setup results in operands data member
    *             List[Operand] is a from the current node of game tree execution to the tree root
    *             head of the list is the current operand
    */
  def foreachPost(calc: List[Operand] => Unit): Operand = {
    currentCalculation += 1
    val op = initialOperand
    foreachPost(calc, List(op))
    op
  }

  def foreachPostPar(calc: List[Operand] => Unit): Operand = {
    currentCalculation += 1
    val op = initialOperand
    foreachPostPar(calc, List(op))
    op
  }

  private def initialOperand: Operand = new Operand(
     null,
      root,
      nextPathElems(initial.moves,1),
     0)

  private def treeNode(state: State, level: Int): Node = {
    if (nodes.size >= maxStates || level > maxDepth * 2 + 2) {
      val n = nodes.get(state)
      if (n != null) n else new Node(state)
    } else {
      nodes.computeIfAbsent(state, (s) => new Node(s))
    }
  }

  private def operand(ops: List[Operand], move: Move, node: Node): Operand = {
    val newLevel = ops.head.level + 1
    new Operand(move,
      node,
      nextPathElems(expand(ops, node, newLevel), newLevel),
      newLevel)
  }

  def maxInterestingDepth: Int = 2 * maxDepth

  private def expand(ops: List[Operand], node: Node, level: Int): Map[Action, Seq[Move]] =
    if (node.alreadyDone(level)) Map.empty
    else if (node.state.gameOver) Map.empty
    else if (level <= maxDepth) node.state.moves
    else if (level > maxInterestingDepth) Map.empty
    else if (ops.exists((op: Operand) => node.state == node.state && op.level >= maxDepth)) Map.empty
    else if (node.state.interesting) node.state.moves
    else {
      val moves = node.state.moves
      if (moves.size < ops.head.next.size) moves
      else {
        val nonEnd = moves.values.flatten.count(!_.state.gameOver)
        if (nonEnd <= 1 || nonEnd < ops.head.next.values.flatten.count(!_._1.state.gameOver))
          moves
        else Map.empty
      }
    }

  private def nextPathElem(move: Move, level: Int): (Move, Node) = {
    val node: Node = treeNode(move.state, level)
    val mm = move.normalize(node.state)
    (mm, node)
  }

  private def nextPathElems(moves: Map[Action, Seq[Move]], level: Int): Map[Action, Seq[(Move, Node)]] =
    for ((a, ms) <- moves) yield a -> ms.map(m => nextPathElem(m, level))

  private def foreachPost(calc: List[Operand] => Unit, ops: List[Operand]): Unit = {
    val old = ops.head
    if (old.node.alreadyDone(old.level)) () // already done with even better accuracy
    else {
      val old = ops.head
      for ((move, node) <- old.next.values.flatten)
        foreachPost(calc, operand(ops, move, node) :: ops)
      calc(ops)
      old.node.markDone(old.level)
    }
  }

  private def foreachPostPar(calc: List[Operand] => Unit, ops: List[Operand]): Unit = {
    val old = ops.head
    if (old.node.alreadyDone(old.level)) () // already done with even better accuracy
    else {
      val old = ops.head
      for (ms <- old.next.values.par)
        for ((move, node) <- ms.par)
          foreachPostPar(calc, operand(ops, move, node) :: ops)
      old.node.synchronized {
        if (!old.node.alreadyDone(old.level)) {
          calc(ops)
          old.node.markDone(old.level)
        }
      }
    }
  }
}